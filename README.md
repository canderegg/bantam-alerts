# bantam-alerts

## Purpose

The `bantam-alerts` module implements a set of Bootstrap-like alert boxes for BantamJS. Each is a div element styled with the `alert` and alert type classes.

## Installation

The CSS can be downloaded [here](files/1.0/bantam-alerts-1.0.min.css) and linked directly into your app. Alternatively, `bantam-alerts` is a module available through the [Bantam Coop](https://canderegg.gitlab.io/bantam-coop/) module manager.

To install the module through Bantam Coop, use the following command:

```bash
$ python coop.py add bantam-alerts +1.0
```

## Usage

There are four types of alert boxes available in the module, each of which can be used for a different purpose.

![Alert boxes](alerts.png)

Each alert box is made up of a single div element with the `alert` CSS class applied. In addition to this base alert class, another class denoting the alert type is applied: `alert-info`, `alert-success`, `alert-warning`, or `alert-danger`. This second class specifies the color styling of the alert box.

A code example with each alert type might be implemented as follows:

```html
<div class="alert alert-info">
This is an alert box styled with the <tt>alert-info</tt> class.
</div>

<div class="alert alert-success">
This is an alert box styled with the <tt>alert-success</tt> class.
</div>

<div class="alert alert-warning">
This is an alert box styled with the <tt>alert-warning</tt> class.
</div>

<div class="alert alert-danger">
This is an alert box styled with the <tt>alert-danger</tt> class.
</div>
```

## License

This module is distributed under the [MIT License](LICENSE).
